package com.linkdev.indoornavigation.fragment;

import android.widget.Toast;

import com.pointrlabs.core.dataaccess.datamanager.models.Point;
import com.pointrlabs.core.dataaccess.models.poi.Poi;
import com.pointrlabs.core.map.model.ContainerFragmentState;
import com.pointrlabs.core.map.model.MapMode;
import com.pointrlabs.core.map.ui.SimpleLineDrawable;
import com.pointrlabs.core.nativecore.wrappers.Plog;
import com.pointrlabs.core.positioning.model.Location;

import java.util.ArrayList;
import java.util.List;

public class GEMContainer extends BaseContainerFragment {

    private static GEMContainer instance;
    public static GEMContainer newInstance() {
        if(instance == null){
            instance = new GEMContainer();
        }
        return instance;
    }

    @Override
    public void transitStateTo(ContainerFragmentState state) {
        Plog.v( "Transit state to - " + state);
        if (stateChangeListener != null) {
            stateChangeListener.onDisplayStateChanged(state);
        }

        previousState = this.state;
        this.state = state;
        switch (state) {
            case Map:
                if (getLocateMeButton() != null) {
                    getLocateMeButton().detectPositionOnScreen(false);
                }

                setComponentVisibility(false,
                        getMapModeSwitcher(),
                        getLevelPicker(),
                        getNavigationFooter(),
                        getTurnByTurnHeader(),
                        getLocateMeButton(),
                        getSearchView());
                break;
            case Search:
                if (getLocateMeButton() != null) {
                    getLocateMeButton().detectPositionOnScreen(true);
                    getLocateMeButton().setVisibility(!isPinOnScreen.get());
                }
                if (getLevelPicker().getShouldBeVisible()) {
                    setComponentVisibility(true,getLevelPicker());
                }

                setComponentVisibility(false,
                        getNavigationFooter(),
                        getTurnByTurnHeader());
                setComponentVisibility(true,
                        getSearchView(),
                        getMapModeSwitcher());  //debb move map mode switcher to false
                break;
            case PoiSelected:
                if (getLocateMeButton() != null) {
                    getLocateMeButton().detectPositionOnScreen(false);
                }
                if (getLevelPicker().getShouldBeVisible()) {
                    setComponentVisibility(true,getLevelPicker());
                }

                setComponentVisibility(false,
                        getMapModeSwitcher(),
                        getTurnByTurnHeader(),
                        getLocateMeButton(),
                        getSearchView());
                setComponentVisibility(true,
                        getNavigationFooter());
                if (getNavigationFooter() != null) {
                    getNavigationFooter().setPathfindingActive(true);
                }
                break;
            case PathfindingHeader:
                map.getMapModeCoordinator().setMapMode(MapMode.PathTracking);
                if (getLocateMeButton() != null) {
                    getLocateMeButton().detectPositionOnScreen(false);
                }
                if (getNavigationFooter() != null) {
                    getNavigationFooter().setPathfindingActive(false);
                }
                if (getLevelPicker().getShouldBeVisible()) {
                    setComponentVisibility(true,getLevelPicker());
                }

                setComponentVisibility(false,
                        getMapModeSwitcher(),
                        getLocateMeButton(),
                        getSearchView());
                setComponentVisibility(true,
                        getNavigationFooter(),
                        getTurnByTurnHeader());

                scaleToCurrentLocation(getQuarterZoomValue());
                break;
            case PathfindingHeaderAndFooter:
                map.getMapModeCoordinator().setMapMode(MapMode.PathTracking);
                if (getLocateMeButton() != null) {
                    getLocateMeButton().detectPositionOnScreen(false);
                }
                if (getLevelPicker().getShouldBeVisible()) {
                    setComponentVisibility(true,getLevelPicker());
                }
                if (getNavigationFooter() != null) {
                    getNavigationFooter().setPathfindingActive(false);
                }

                setComponentVisibility(false,
                        getMapModeSwitcher(),
                        getLocateMeButton(),
                        getSearchView());
                setComponentVisibility(true,
                        getNavigationFooter(),
                        getTurnByTurnHeader());
                break;
        }
    }

    @Override
    public void onTriggerPoiEntered(Poi poi) {
        //do nothing
        Plog.d("Trigger entered : "+poi.getName());
        SimpleLineDrawable lineDrawable = new SimpleLineDrawable();
        lineDrawable.setIdentifier("trigger");
        List<Location> pointsArray = new ArrayList<>();
        for(Point point : poi.getRegion()){
            pointsArray.add(new Location((float)point.getX(),(float)point.getY(),0,1,0));
        }
        lineDrawable.setPointsArray(pointsArray);
        map.addDrawable(lineDrawable);

        map.getActivity().runOnUiThread(() -> {
            Toast.makeText(getContext(), "Trigger entered : "+poi.getName(), Toast.LENGTH_SHORT).show();
        });


    }

    @Override
    public void onTriggerPoiExited(Poi poi) {
        Plog.d("Trigger exited : "+poi.getName());
        map.getActivity().runOnUiThread(() -> {
            Toast.makeText(getContext(), "Trigger exited : "+poi.getName(), Toast.LENGTH_SHORT).show();
        });
    }

}
