package com.linkdev.indoornavigation;

import android.content.Context;
import android.content.Intent;

import com.linkdev.indoornavigation.activity.PointrMapActivity;
import com.pointrlabs.core.license.LicenseKey;
import com.pointrlabs.core.management.Pointr;
import com.pointrlabs.core.nativecore.wrappers.Plog;

/**
 * Created by Sherif.ElNady on 1/13/2019.
 */

public class IndoorNavigation {

    public static void start(Context context) {
        String string = context.getResources().getString(R.string.pointr_licence);
        Pointr.with(context, new LicenseKey(string), Plog.LogLevel.VERBOSE);

        context.startActivity(new Intent(context, PointrMapActivity.class));
    }
}
