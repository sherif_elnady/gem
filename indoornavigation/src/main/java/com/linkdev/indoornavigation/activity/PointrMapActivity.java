package com.linkdev.indoornavigation.activity;

import android.support.annotation.NonNull;

import com.linkdev.indoornavigation.fragment.BaseContainerFragment;
import com.linkdev.indoornavigation.fragment.GEMContainer;
import com.pointrlabs.core.management.GeofenceManager;
import com.pointrlabs.core.management.Pointr;
import com.pointrlabs.core.management.PositionManager;
import com.pointrlabs.core.management.models.Facility;
import com.pointrlabs.core.positioning.model.CalculatedLocation;
import com.pointrlabs.core.positioning.model.GeoPosition;

import java.util.EnumSet;


/**
 * Created by deliganli on 29.09.2016.
 */

public class PointrMapActivity extends BasePointrMapActivity
        implements PositionManager.Listener, GeofenceManager.Listener {

    @Override
    protected BaseContainerFragment getContainerFragment() {
        return GEMContainer.newInstance();
    }

    @Override
    protected void onPointrStateUpdated(Pointr.State state) {
        if (state == Pointr.State.RUNNING) {
            registerPointrListeners();
        } else if (state == Pointr.State.OFF) {
            unregisterPointrListeners();
        }
    }

    //region Internal methods

    /**
     * Registers listeners for Pointr managers
     *
     * @warn It must be called when Pointr is running (to ensure managers are available)
     */
    private void registerPointrListeners() {
        PositionManager posM = Pointr.getPointr().getPositionManager();
        posM.addListener(this);

        GeofenceManager geoM = Pointr.getPointr().getGeofenceManager();
        geoM.addListener(this);
    }

    /**
     * Tries to unregister listeners for Pointr managers
     */
    private void unregisterPointrListeners() {
        PositionManager posM = Pointr.getPointr().getPositionManager();
        if (posM != null) {
            posM.removeListener(this);
        }
        GeofenceManager geoM = Pointr.getPointr().getGeofenceManager();
        if (geoM != null) {
            geoM.removeListener(this);
        }
    }
    //endregion

    //region PositionManager.Listener

    @Override
    public void onLocationCalculated(CalculatedLocation calculatedLocation) {
        // Do nothing
    }

    @Override
    public void onLevelChanged(int level) {
        // Do nothing
    }

    @Override
    public void onGeolocationCalculated(GeoPosition location) {
        // Do nothing
    }

    @Override
    public void onStateChanged(EnumSet<PositionManager.State> set) {
        // Do nothing
    }

    @Override
    public void onPositionIsFading() {
        // Do nothing
    }

    @Override
    public void onPositionIsLost() {
        // Do nothing
    }
    //endregion

    //region GeofenceManager.Listener methods
    @Override
    public void onEnterFacility(@NonNull Facility facility) {
        // Do nothing
    }

    @Override
    public void onExitFacility(@NonNull Facility facility) {
        // Do nothing
    }
    //endregion
}
